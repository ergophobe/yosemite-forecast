<p class="forecast-notes-trigger"><a href="#" class="button round" data-reveal-id="forecast-notes-details">Forecast Notes</a><p>
<div id="forecast-notes-details" class="reveal-modal" data-reveal style="display:none;">
  <h3><?php print $label; ?></h3>
  <?php print $items[0]['#markup']; ?>
  <a class="close-reveal-modal">&#215;</a>
</div>