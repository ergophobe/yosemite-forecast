<?php
/**
 * @file
 */

// Drupal needs this file.

include_once('iw_admin_tweaks.features.inc');

/**
 * Implements hook_menu_local_tasks_alter() to give better content type admin
 */
function iw_admin_tweaks_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  global $user;

  // assign active_type if view has a content type filter
  $active_type = FALSE;
  if((arg(0) != 'admin') && isset($router_item['page_callback']) &&  ($router_item['page_callback'] == 'views_page')) {
    //$view = views_page($router_item['page_arguments'][0], $router_item['page_arguments'][0]);
    $view = views_get_view($router_item['page_arguments'][0]);
    $tfilter = FALSE; 

    if (isset($view->display['default']->display_options['filters']['type'])) {
      $tfilter = $view->display['default']->display_options['filters']['type'];
    }
    $display = $router_item['page_arguments'][1];
    if (is_array($display)) {
      $display = array_shift($display);
    }
    if (isset($view->display[$display]->display_options['filters']['type'])) {
      $tfilter = $view->display[$display]->display_options['filters']['type'];
    }
    if ($tfilter) {
      if (($tfilter['table'] == 'node')) {
        if (!isset($tfilter['operator']) || ($tfilter['operator'] == 'in')) {
          $active_type = array_shift($tfilter['value']);
        }
      }
    }
  }
  
  // set active_type if iw_admin_tweaks view
  if (($root_path == 'nodes' || $root_path == 'node')) {
    $active_type = arg(1);
  }
  // set active_type if iw_admin_tweaks view
  if (($user->uid > 0) && ($root_path == 'node/%')) {
  	$node = node_load(arg(1));
    $active_type = $node->type;
  }

  
  if ($active_type) {
    $type = node_type_load($active_type);
    if (!isset($type->name)) {
      return;
    }
    $content_name = strtolower($type->name);
    $item = menu_get_item('node/add/' . str_replace('_', '-', $active_type));

    if ($item['access']) {
      $item['title'] = t('Add another @type_name', array('@type_name' => $content_name));
      $item['localized_options']['attributes']['class'][] = 'button'; //this save us from doing merge with existing array
      $item['localized_options']['attributes']['class'][] = 'round';  // but if we add lots of classes, easier to merge
      $item['localized_options']['attributes']['class'][] = 'tiny';
      $data['actions']['output'][] = array(
        '#theme' => 'menu_local_action',
        '#link' => $item,
      );
    }
    $href = 'admin/content/node/' . $active_type;
    $item = menu_get_item($href);

    if ($item['access']) {
      $item['href'] = $href;
      $item['title'] = t('Administer all "@type_name" content', array('@type_name' => $content_name));
      $item['localized_options']['attributes']['class'][] = 'button'; //this save us from doing merge with existing array
      $item['localized_options']['attributes']['class'][] = 'round';  // but if we add lots of classes, easier to merge
      $item['localized_options']['attributes']['class'][] = 'tiny';
      $data['actions']['output'][] = array(
        '#theme' => 'menu_local_action',
        '#link' => $item,
      );
    }
  }

  if ($root_path == 'admin/content' || (strpos($root_path, 'admin/content/node') === 0)) {
  	drupal_add_css(drupal_get_path('module', 'iw_admin_tweaks') . '/css/iw_admin_tweaks.content_admin.css');
  	$custom_view = ($root_path != 'admin/content') ? TRUE : FALSE;
    $active_type = arg(3);
    // if active type, clear + add content action
    if (isset($active_type)) {
    	unset($data['actions']['output'][0]);
    }
    $types = node_type_get_names();
    // add contextual add content type link
    if (isset($active_type) && $active_type) {
      $type_name = strtolower($types[$active_type]);
      $item = menu_get_item('node/add/' . str_replace('_', '-', $active_type));
      $item['title'] = t('Add @type_name', array('@type_name' => $type_name));
      $data['actions']['output'][] = array(
        '#theme' => 'menu_local_action',
        '#link' => $item,
      );
    }
    // add all sub-tab if on content to trigger display of subtabs on some themes (e.g. Rubik)
    $href = 'admin/content/node';
    $item = menu_get_item($href);    
    if ($item['access']) {
      $item['href'] = $href;
      $item['title'] = ' ' . t('All');
      $output = array(
        '#theme' => 'menu_local_task',
        '#link' => $item,
      );
      if (!isset($active_type)) {
        $output['#active'] = TRUE;
      }
      $data['tabs'][1]['output'][] = $output; 
    }  
    // add sub-tabs for content types
    $count = count($types) + 1;
    $data['tabs'][1]['count'] = $count;
    $i = 0;
    foreach ($types AS $type => $name) {
      $href = 'admin/content/node/' . $type;
      $item = menu_get_item($href);
      // if custom admin view exists for content type, skip adding sub-tab
      if ($item['path'] != 'admin/content/node') {
        continue;
      }
      
      if ($item['access']) {
        $item['href'] = $href;
        $item['title'] = $name;
        $output = array(
            '#theme' => 'menu_local_task',
            '#link' => $item,
        );
        if ($type == $active_type) {
          $output['#active'] = TRUE;
        }
        $data['tabs'][1]['output'][] = $output;
      }
      $i++;
    } 
    usort($data['tabs'][1]['output'], '_iw_admin_tweaks_content_sort_tabs'); 
  } 
  
}

function _iw_admin_tweaks_content_sort_tabs($a, $b) {
  return strnatcmp($a['#link']['title'], $b['#link']['title']);
}

/**
 * Implement hook_form_alter()
 * @param unknown_type $form
 * @param unknown_type $form_state
 * @param unknown_type $form_id
 *
 * Add ability to move fields to right hand column
 */
function iw_admin_tweaks_form_alter(&$form, &$form_state, $form_id) {

	//dsm(LANGUAGE_NONE);
	if ($form_id == 'field_ui_field_edit_form') {
	  $options = array(
	    '' => t('Standard'),
	    'sidebar' => t('Sidebar'),
	    //'verticaltabs' => t('Vertical tabs'),
	  );
	  $field_name = $form['#field']['field_name'];
//dsm($form_state);
	  $default = '';
	  if (isset($form_state['build_info']['args'][0]['iw_admin_edit_form_display'])) {
	  	$default = $form_state['build_info']['args'][0]['iw_admin_edit_form_display'];
	  }
	  //if (isset($form_state['field'][$field_name][LANGUAGE_NONE]['instance']['iw_admin_edit_form_display'])) {
    //  $default = $form_state['field'][$field_name][LANGUAGE_NONE]['instance']['iw_admin_edit_form_display'];
    //}
	  $form['instance']['iw_admin_edit_form_display'] = array(
	    '#type' => 'radios',
	    '#title' => t('Edit form display location'),
	    '#description' => t('Select where you want this field to display on the edit form. Sidebar is recommended for meta data.'),
	    '#default_value' => $default,
	    '#options' => $options,
	  );
	  //$form['#submit'][] = 'iw_admin_tweaks_form_field_ui_field_edit_form_submit';		
	}
	// use JavaScript to move 
	elseif (isset($form['#node_edit_form'])) {
    // if admin theme is Rubik, enhance node admin
		$theme = variable_get('admin_theme', 'bartik');
		$node_admin = variable_get('node_admin_theme', 'bartik');
		if (($theme != 'rubik') || (!$node_admin)) {
			return;
		}
		drupal_add_js(drupal_get_path('module', 'iw_admin_tweaks') . '/js/iw_admin_tweaks.edit_form.js');
		drupal_add_css(drupal_get_path('module', 'iw_admin_tweaks') . '/css/iw_admin_tweaks.edit_form.css');
		if (isset($form_state['field']) && is_array($form_state['field'])) {
		  foreach ($form_state['field'] AS $name => $field) {
		    if (!isset($field[LANGUAGE_NONE]['instance'])) {
		      continue;
		    }
		  	$field[LANGUAGE_NONE]['instance'] += array(
		  	  'iw_admin_edit_form_display' => '',
		  	);
		  	$display = $field[LANGUAGE_NONE]['instance']['iw_admin_edit_form_display'];
		  	if ($display == 'sidebar') {
          $form[$name]['#attributes']['class'][] = 'display_sidebar';
          // set field to scrollable if checkboxes or radios
          if (isset($form[$name][LANGUAGE_NONE]['#type']) && in_array($form[$name][LANGUAGE_NONE]['#type'], array('checkboxes', 'radios'))) {
          	$form[$name][LANGUAGE_NONE]['#attributes']['class'][] = 'scrollable';
          }
		  	}
		  	// if closed vocabulary, add + add term link to field
		  	$fdata = $form_state['field'][$name][LANGUAGE_NONE];
		  	if (($fdata['field']['type'] == 'taxonomy_term_reference')  && ($fdata['instance']['widget']['type'] == 'options_buttons')) {
		  		$vocab_name = $fdata['field']['settings']['allowed_values'][0]['vocabulary'];

		  	  $item = menu_get_item("admin/structure/taxonomy/$vocab_name/add");
			    if ($item['access']) {
			    	$form[$name][LANGUAGE_NONE]['#attributes']['class'][] = 'with-action-links';
			    	// TODO work out styling
			      $form[$name][LANGUAGE_NONE]['#field_suffix'] = '<ul class="action-linksx"><li>' . l('+ ' . t('Add @name term', array('@name' => $vocab_name)), "admin/structure/taxonomy/$vocab_name/add", array('html' => TRUE, 'attributes' => array('target' => "_blank", 'class' => array('add-term-link')))) . '</li></ul>';
			      
			    }
		  	}
		  }
		}
		// move schedule
		if (isset($form['scheduler_settings'])) {
			unset($form['scheduler_settings']['#group']);
			unset($form['scheduler_settings']['#attached']);
			$form['scheduler_settings']['#title'] = $form['scheduler_settings']['publish_on']['#title'];
			unset($form['scheduler_settings']['publish_on']['#title']);
			$form['scheduler_settings']['#attributes']['class'][] = 'display_sidebar';	
			$form['scheduler_settings']['#weight'] = -10;
		}
		if (isset($form['metatags'])) {
			$form['metatags']['#weight'] = -20;
			
		}
	}
}