Both Zurb Foundation Theme and Navbar (Mobile Friendly Administration Toolbar) use Modernizr with some overlaop in functions. Normally Foundation loads it from the js/vendor folder via the .info file, but it's already loaded by Navbar from the library. So we need to block loading by Foundation (comment out in .info) and add the components unique to Foundation into our library version. If we turn off Navbar, we also lose Modernizr loaded via the library, so Navbar becomes sort of a soft dependency for Foundation.

These URLs create the preconfigured downloads needed for each, along with the combined version.
Navbar
http://modernizr.com/download/#-inputtypes-svg-touch-cssclasses-addtest-teststyles-prefixes-elem_details
(unique: -addtest, -elem_details

Foundation
http://modernizr.com/download/#-inlinesvg-svg-svgclippaths-touch-shiv-mq-cssclasses-teststyles-prefixes-ie8compat-load
(unique: inlinesvg, svgclippaths, shiv, mq, ie8compat, load)

Combined
http://modernizr.com/download/#-inputtypes-inlinesvg-svg-svgclippaths-touch-shiv-mq-cssclasses-addtest-teststyles-prefixes-elem_details-ie8compat-load